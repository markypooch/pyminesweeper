"""
    CIS-236-5001
    CIS Project
    David Klick
    Marcus Hansen

    Project I - Python adaptation of MineSweeper
    2/2/19
"""
from   Game.pyGame.pygame_mainloop import *
from   Game.pyTK.tk_gameboard      import TKGameboard
import argparse, tkinter as tk

if __name__ == "__main__":

    # initialize tkinter. If we selected the pygame version of PyTNTSweeper, than Tkinter is only
    # used to validate that our primary monitor resolution is greater than 800x800
    root = tk.Tk()
    if root.winfo_screenwidth() < 800 or root.winfo_screenheight() < 800:
        print("Screen dimensions of primary monior must be greater than 800x800")
    else:
        # define debug, and pygame as our optional command line arguments
        parser = argparse.ArgumentParser(description='MineSweeper clone')
        parser.add_argument("pygame", help="enable pygame version (yes/no)",
                            nargs='?',
                            const=1,
                            default=1,
                            type=str)
        parser.add_argument("debug", help="display mine locations (yes/no)",
                            nargs='?',
                            const=1,
                            default=0,
                            type=str)
        
        # by default, pygame is enabled, and debug is disabled.
        args = parser.parse_args()

        # cast them to ints
        pygame_version = int(args.pygame)
        debug          = int(args.debug)

        # TKinter abstracts away the detail of handling a message pump with a main loop, so we just invoke our
        # class's constructor directly, if pygame was selected, we invoke our mainloop method to handle window messages/input/gamelogic
        if pygame_version:
            pygame_mainloop(debug)
        else:
            tk_game_board = TKGameboard(root, debug)