# PyMineSweeper

A python clone of the minesweeper classic!

## Install/Development Environment

Ensure you have python 3/pip installed, and added to your environment path variable

## Virtual Environments

Virtual environments make it easy to compartmentalize inter-python project dependencies on the same machine
it is _HIGHLY_ recommended that they are used

1. If not installed already, install virtual environments through pip, `pip install virtualenv`
2. setup a virtual environment, i.e `python -m virtualenv venv`
3. start that vrtual environment with:
    * Bash `source /venv/bin/activate`
    * DOS  `.\venv\Scripts\activate`
4. clone the repository w/ `git clone git@gitlab.com:markypooch/pyminesweeper.git "your_local_folder"`
5. navigate to the root of that repository and perform a `pip install -r requirements.txt` to install the project depedencies
6. Finally, perform a `python entry.py`!
7. Optional Arguments are available for different versions & debugging
    * For pygame w/ debugging `python entry.py 1 1`
    * For tkinter version      `python entry.py 0`
    * For tkinter version w/ debugging `python entry.py 0 1`