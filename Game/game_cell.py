class Cell:

    # item, right now really only holds a "MINE", but the idea is to expand it to
    # other items
    item               = ""

    # the position in the array, how mines it touches, and if it's an edge cell (touches a mine)
    x_pos, y_pos       = 0, 0
    touching_mines_num = 0
    edge               = False

    def __init__(self, x, y):
        
        # set our position, and initial state
        self.x_pos = x
        self.y_pos = y

        self.state = {
            "ACTIVE":True,
            "ANIMATING":False,
            "DEACTIVATED":False,
        }

    def set_state(self, previous_state, state):

        # transition state, useful for animation
        self.state[previous_state] = False
        self.state[state]          = True

    # set the item type, right now, only "MINE"
    def set_item(self, item_type):
        self.item = item_type