from   Game.game_board        import GameBoard
from   Game.pyTK.tk_constants import *
from   Game.game_constants    import *
import tkinter as tk

class TKGameboard(GameBoard):

    __tk_object       = None
    __tk_window       = None
    __cell_buttons    = []

    __debug           = 0

    # our gamestate
    __game_condition = {'PLAY':True, 'WIN':False, 'LOSE':False}

    def __init__(self, root, debug):
        super(TKGameboard, self).__init__()

        # init tkinter, set the window size, and set the title
        self.__tk_object = root
        self.__tk_object.title("PyTNTSweeper")
        self.__tk_object.geometry(str(WINDOW_HEIGHT) + 'x' + str(WINDOW_WIDTH))
        self.__tk_object.resizable(False, False)

        self.__tk_window = tk.Frame(master=self.__tk_object)

        # cache our debug flag
        self.__debug     = debug

        # create our cell buttons, using a lambda to set the callback while simulatenously passing the index of 
        # the loop to futher reference when the onclick callback is triggered
        for i in range(len(self.cell_list)):
            self.__cell_buttons.append(tk.Button(master=self.__tk_object, 
                                                 height=CELL_HEIGHT, 
                                                 width=CELL_WIDTH, 
                                                 command = lambda idx = i: self.onclick(idx)))

            # set the position relative to the underlying cell position
            self.__cell_buttons[-1].grid(row=self.cell_list[i].y_pos, column=self.cell_list[i].x_pos)
        
        # determine based on debug flag to show mine location
        self.__show_mine_location(self.__game_condition)

        # invoke tkinters message pump/loop
        self.__tk_object.mainloop()

    def onclick(self, idx):
        if self.__game_condition['PLAY']:
            
            # deactivate the cell clicked
            self.__deactivate_cell(idx)

            # test if more cells need to be flipped, and call deactivate cell accordingly
            cells = self.compute_deactivated_cells(idx)
            for cell in cells:
                self.__deactivate_cell(WIDTH * cell.x_pos + cell.y_pos)

    def __deactivate_cell(self, idx):

        # set the cell from ACTIVE, to DEACTIVATED (skipping ANIMATING which is reserved for the pygame version)
        self.cell_list[idx].set_state("ACTIVE", "DEACTIVATED")
        self.__cell_buttons[idx].configure(bg="lightgray")
        
        # check the currrent game condition
        self.__game_condition = self.check_win_condition()
        if self.__game_condition['LOSE']:
            self.__tk_object.title("PyTNTSweeper - it's dead, jim...")
        elif self.__game_condition['WIN']:
            self.__tk_object.title("PyTNTSweeper - Winner Winner, Sheen dinner!")

        self.__show_mine_location(self.__game_condition)

        # don't print the 'touching_mine_num' variable if it isn't 0, and if not, set the text of the button
        if self.cell_list[idx].touching_mines_num > 0:
            self.__cell_buttons[idx].configure(text=str(self.cell_list[idx].touching_mines_num))

    def __show_mine_location(self, game_condition):

        # if debug is enabled, or we have won, or lost show the mine locations
        if game_condition['LOSE'] or game_condition['WIN'] or self.__debug:
            for cell in self.cell_list:
                if cell.item == "MINE":
                    self.__cell_buttons[WIDTH * cell.x_pos + cell.y_pos].configure(bg="black")


        
