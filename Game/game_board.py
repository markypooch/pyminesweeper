from   Game.game_cell      import *
from   Game.game_constants import *
import random

class GameBoard:

    # our constants for this class
    game_conditions   = {"WIN":False, "LOSE":False, "PLAY":True, "QUIT":False}

    def __init__(self):

        # our lookup map to loop through when we need to search the area around a cell
        self.__lookup_map       = ((1, 0), (-1, 0), (0, 1), (0, -1), (1, 1), (1, -1), (-1, -1), (-1, 1))

        # define the size of our cell list relative to the (h*w) and populate our board
        self.cell_list          = [None]*(HEIGHT*WIDTH)
        self.__build_board()

    def __build_board(self):

        # instantiate our cells, and assign their positions on the map
        for cell_height in range(HEIGHT):
            x_pos = 0
            for cell_width in range(WIDTH):
                self.cell_list[self.__get_pos(cell_width, cell_height)] = Cell(x_pos, cell_height)
                x_pos += 1

        # randomly place mines in our map, making sure we don't "double" place
        cells_assigned_mines = []
        for i in range(NUMBER_OF_MINES):
            cell = random.randint(0, (HEIGHT*WIDTH)-1)

            while cell in cells_assigned_mines:
                cell = random.randint(0, (HEIGHT*WIDTH)-1)
            
            cells_assigned_mines.append(cell)
            self.cell_list[cell].set_item("MINE")
        
        # compute the mine adjacencies, that is, for each cell, find out how many, _if any_
        # mines surround it
        self.compute_mine_adjacencies()
        
    # helper method so we don't need to type this expression everywhere when indexing into the 
    # list
    def __get_pos(self, x, y):
        return WIDTH * x + y

    # helper method to test if the offsets used when testing for mine adjacency, or for deactivated
    # cells exceeds the bounds of our map
    def __exceeds_offsets(self, cell, x_offset, y_offset):
        exceeds = False
        if cell.x_pos+x_offset >= WIDTH or cell.x_pos+x_offset < 0:
            exceeds = True
        if cell.y_pos+y_offset >= HEIGHT or cell.y_pos+y_offset < 0:
            exceeds = True
        
        return exceeds

    def compute_mine_adjacencies(self):

        # loop through the map and offset each cells, "x_pos" & "y_pos" to find out if it's next to a mine
        for cell in self.cell_list:
            cell.touching_mines_num = 0
            for lookup_coordinate in self.__lookup_map:

                # make sure we don't exceed the map
                x_offset, y_offset = lookup_coordinate[0], lookup_coordinate[1]
                if self.__exceeds_offsets(cell, x_offset, y_offset):
                    continue

                # test if the offsets find a mine, if they do, add to our total for this cell
                if self.cell_list[self.__get_pos(cell.x_pos+x_offset, cell.y_pos+y_offset)].item == "MINE":
                    self.cell_list[self.__get_pos(cell.x_pos, cell.y_pos)].touching_mines_num += 1
    
    # helper method to verify that the deactivated cell isn't doubly added to the list
    def is_cell_in_list(self, deactivated_cells, x_pos, y_pos):
        for cell in deactivated_cells:
            if x_pos == cell.x_pos and y_pos == cell.y_pos:
                return True
        return False

    # when a player, "clicks" on a cell that touches, "0" mines, we want to continually flip the cells 
    # in proximity of said cell that also, _don't_ touch any mines. 
    def compute_deactivated_cells(self, cell_collision_index):

        deactivated_cells = []

        # if the count of touching mines is not greater than 0
        if not self.cell_list[cell_collision_index].touching_mines_num > 0:

            # append the cell that was clicked to the deactivated list
            deactivated_cells.append(self.cell_list[cell_collision_index])

            # for each cell in the deactivated list, test if it's a "edge"
            for cell in deactivated_cells:
                if cell.edge == False:

                    # if not an edge, perform the lookup and find out one of two things:
                    #     * if the cell has no mines touching it, mark it as deactivated
                    #     * if the cell has mines attached to it, mark it as deactivated, and flip it's "edge" flag
                    for lookup_coordinate in self.__lookup_map:
                        x_offset, y_offset = lookup_coordinate[0], lookup_coordinate[1]
                        if self.__exceeds_offsets(cell, x_offset, y_offset):
                            continue

                        # in both conditions we want to validate that we don't doubly add cells to our deactivated list
                        # since we will likely "re-encounter" them when doing our search
                        if self.cell_list[self.__get_pos(cell.x_pos+x_offset,  cell.y_pos+y_offset)].touching_mines_num <= 0:
                            if not self.is_cell_in_list(deactivated_cells, cell.x_pos+x_offset, cell.y_pos+y_offset):
                                deactivated_cells.append(self.cell_list[self.__get_pos(cell.x_pos+x_offset,  cell.y_pos+y_offset)])
                        else:
                            self.cell_list[self.__get_pos(cell.x_pos+x_offset,  cell.y_pos+y_offset)].edge = True
                            if not self.is_cell_in_list(deactivated_cells, cell.x_pos+x_offset, cell.y_pos+y_offset):
                                deactivated_cells.append(self.cell_list[self.__get_pos(cell.x_pos+x_offset,  cell.y_pos+y_offset)])

        # if we found no cells that meet the criteria for being deactivated, add the cell that was clicked
        # as the only cell to deactivate 
        if len(deactivated_cells) == 0:
            self.cell_list[cell_collision_index].edge = True
        return deactivated_cells

    # check if we won, lost, or neither
    def check_win_condition(self):

        number_of_deactivated_cells = 0

        # for each cell, check its state
        for cell in self.cell_list:
            if cell.state['DEACTIVATED'] or cell.state['ANIMATING']:
                if cell.item == "MINE":
                    # if a cell is both deactivated, AND a mine, we lost
                    self.game_conditions['LOSE'] = True
                    self.game_conditions['PLAY'] = False
                    break
                else:
                    number_of_deactivated_cells += 1
        else:

            # we didn't encounter a "Lose" condition, so time to check if we won, or the game is ongoing

            # if the number_of_deactivated cells is equal to the size of our cells list _MINUS_ the 
            # constant for the number of mines, than we have won
            if number_of_deactivated_cells == (len(self.cell_list)-NUMBER_OF_MINES):
                self.game_conditions["WIN"] = True
                self.game_conditions['PLAY'] = False

            else:
                # else, the game goes on
                self.game_conditions["PLAY"] = True

        return self.game_conditions




