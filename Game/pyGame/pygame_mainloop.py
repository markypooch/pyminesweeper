import pygame
from   Game.pyGame.pygame_gameboard import *

# initialize pygame with a fixed resolution of 800x800
def pygame_init():
    pygame.init()
    display = pygame.display.set_mode((800, 800))
    
    pygame.display.set_caption("PyTNTSweeper")
    return display

def pygame_mainloop(debug):

    # initialize pygame, and our pygame board
    display = pygame_init()
    board   = PyGameBoard(display, debug)

    # our gamestate
    game_condition = {'PLAY':True, 'QUIT':False}
    getTicksLastFrame = 0.0
    while True:
        
        toggle_event = False
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                game_condition['QUIT'] = True
                break

        if game_condition['QUIT']:
            break

        # get the miliseconds that it took for the last frame (iteration of this loop) to complete.
        # this is needed for animation
        t = pygame.time.get_ticks()
        deltaTime = (t - getTicksLastFrame) / 1000.0 #scale to mili

        # cache the last time for subsequent calcs
        getTicksLastFrame = t

        if pygame.mouse.get_pressed()[0] != 0 and game_condition['PLAY']: 
            mouse_pos = pygame.mouse.get_pos()
            if not board.test_cell_collision(mouse_pos[0], mouse_pos[1]) == -1:

                game_condition = board.check_win_condition()
                if game_condition['LOSE']:
                    pygame.display.set_caption("PyTNTSweeper - it's dead jim...")
                elif game_condition['WIN']:
                    pygame.display.set_caption("PyTNTSweeper - winner, winner, Sheen dinner!")

        # render our board, and encapsulated cells,tnt, etc.
        board.render(deltaTime)

        # swap the backbuffer to the frontbuffer (i.e. screen) 
        pygame.display.flip()