# our magic values/constants, toggle at your own risk :)
# constants for tracking values for the visual offset into the 
# main arena, and the area between each cell.

# Highly tuned for the right
# "visuals" in the context of spacing. Likely a more flexible solution here
# would be to base these values off of the size of the window instead of being hardcoded. 
SPRITE_ARENA_OFFSET     = 55
SPRITE_CELL_OFFSET      = 34.25

SPRITE_CELL_DIMENSIONS  = (40, 40)
SPRITE_TNT_DIMENSIONS   = (70, 70)
SPRITE_ARENA_DIMENSIONS = (800, 800)
