from   Game.game_board                import *
from   Game.pyGame.load_cell_sprites  import *
from   Game.pyGame.pygame_constants   import *
import pygame, sys

class PyGameBoard(GameBoard):

    # hold a reference to the backbuffer, and sprites needed for the play area
    __display      = None
    __board_sprite = None
    __tnt_sprite   = None
    __flag_sprite  = None

    # a list of cell sprites. This representation is completely separate from the "game_cell"
    # type. The idea being when this code base is expanded to include different graphics options,
    # i.e. Tkinter, or if we feel squirrely OpenGL for 3d rendering
    __cell_sprite  = []

    # track our current state, and debug flag to display tnt
    debug          = 0

    def __init__(self, display, debug):
        super(PyGameBoard, self).__init__()
        
        # store a reference to our display, load the arena image, and scale it to the
        # window
        self.__display      = display
        self.__board_sprite = pygame.image.load("./Game/Assets/arena.png")
        self.__board_sprite = pygame.transform.scale(self.__board_sprite, SPRITE_ARENA_DIMENSIONS)

        # load the "Mine" sprite
        self.__tnt_sprite   = pygame.image.load("./Game/Assets/tnt.png")
        self.__tnt_sprite   = pygame.transform.scale(self.__tnt_sprite, SPRITE_TNT_DIMENSIONS)

        # load the "Flag" sprite
        self.__flag_sprite   = pygame.image.load("./Game/Assets/flag.png")
        self.__flag_sprite   = pygame.transform.scale(self.__flag_sprite, SPRITE_TNT_DIMENSIONS)

        self.debug = debug

        # load the cell textures
        load_cell_textures()

        # for each cell in our list, create another cell in a list dedicated for animation, and rendering
        # this list is COMPLETELY separate from the cell_list as this only pertains to the location of 
        # the original cell
        for cell in self.cell_list:
            self.__cell_sprite.append({
                "texture":texture_list,
                "rect"   :pygame.Rect((cell.x_pos*SPRITE_CELL_OFFSET) + SPRITE_ARENA_OFFSET,
                                      (cell.y_pos*SPRITE_CELL_OFFSET) + SPRITE_ARENA_OFFSET, 
                                      SPRITE_CELL_DIMENSIONS[0],
                                      SPRITE_CELL_DIMENSIONS[1]),
                "dt"     :0.0
            })   

    def test_cell_collision(self, mouse_x, mouse_y):

        # loop through each cell testing it's pygame rect against our mouse coordinates
        cell_collision_index = -1
        for i in range(len(self.__cell_sprite)):
            if self.__cell_sprite[i]['rect'].collidepoint(mouse_x, mouse_y):

                # we got a hit! Cache the index and break, and set the state of the cell to, "Animating"
                cell_collision_index = i
                self.cell_list[cell_collision_index].set_state("ACTIVE", "ANIMATING")

                # test if more cells need to be flipped
                self.compute_cells_in_animation(cell_collision_index)
                
                break        
        return cell_collision_index

    def compute_cells_in_animation(self, cell_collision_index):
        # call the parent method to find out if surrounding cells have no directly attached "mines"
        # if so, return a list of them based upon the mine we clicked
        cell_list = self.compute_deactivated_cells(cell_collision_index)

        # for each cell returned to be, "Deactivated", set it's state to, "Animating"
        for cell in cell_list:
            cell.set_state("ACTIVE", "ANIMATING")

    def animate_cell(self, i, dt):

        # by default, grab the original texture so surface is not, "None"
        # and add the delta, to this particular cell sprite
        surface = self.__cell_sprite[i]["texture"][0]
        self.__cell_sprite[i]['dt'] += dt

        # if the cell sprite is greater than 2.9 we are done animating, transistion the state from
        # "ANIMATING" to "DEACTIVATED"
        if self.__cell_sprite[i]['dt'] >= 0.9:
            surface = self.__cell_sprite[i]['texture'][29]

            # set the cell to deactivated
            self.cell_list[i].set_state("ANIMATING", "DEACTIVATED")
        else:

            # iterate through ever sprite in the list spending ~100ms on every frame of
            # animation, and handle an edge case that shouldn't happen
            indice = int(abs(self.__cell_sprite[i]['dt']/0.033))
            if indice >= 30: indice = 29
            
            # with the defined indice, return _that_ frame in the animation
            surface = self.__cell_sprite[i]["texture"][indice]
        return surface

    def render(self, dt):

        # blit our board sprite onto the backbuffer
        self.__display.blit(self.__board_sprite, (0, 0))

        # loop through each cell and display a texture on the backbuffer relative to it's state
        for i in range(len(self.cell_list)):
            
            cell_texture      = self.__cell_sprite[i]["texture"][0]
            text_on_cell      = None
            if self.cell_list[i].state['ANIMATING']:
                
                # deduce the current frame of animation
                cell_texture = self.animate_cell(i, dt)
            if self.cell_list[i].state['DEACTIVATED']:
                
                cell_texture      = self.__cell_sprite[i]["texture"][29]

                # if the underlying `cell_list` is an edge, (touches a mine) display text on how
                # many mines it touches
                if self.cell_list[i].edge == True:
                    cell_font    = pygame.font.SysFont('Comic Sans MS', 16)
                    text_on_cell = cell_font.render(str(self.cell_list[i].touching_mines_num), False, (0, 0, 0))

            # blit the cell texture
            self.__display.blit(cell_texture, (self.__cell_sprite[i]["rect"].x, self.__cell_sprite[i]["rect"].y))

            # if we uncovered a "Mine", OR debug is defined, blit the TNT texture. And if we have any "text" to render
            # for this particular cell, do so now. 

            # in addition, if we lost, i.e. game_condition lose, or debug is enabled, display all mine locations
            if (self.cell_list[i].item == "MINE" and self.debug                or
                self.cell_list[i].item == "MINE" and not self.game_conditions['PLAY']):
                
                # if we win, swap out the mine texture with the flag texture
                mine_texture = self.__tnt_sprite
                if self.game_conditions['WIN']:
                    mine_texture = self.__flag_sprite

                self.__display.blit(mine_texture, (self.__cell_sprite[i]["rect"].x-20, self.__cell_sprite[i]["rect"].y-20))
            if text_on_cell is not None:
                self.__display.blit(text_on_cell, (self.__cell_sprite[i]["rect"].x+10, self.__cell_sprite[i]["rect"].y+10))