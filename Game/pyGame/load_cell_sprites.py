from   Game.pyGame.pygame_constants import *
import pygame

# A helper method for loading in our cell textures
texture_list = []
def load_cell_textures():
    global texture_list

    # load each cell texture which has the format of, "0001", "0002", "0003"..."0030"
    for i in range(30):
        file_name = "00"
        file_name +=  "0"+str((i+1)) if (i+1) < 10 else str((i+1))
        texture_list.append(pygame.image.load("./Game/Assets/CellStone/"+file_name+".png"))
    
    # scale each cell texture relative to the defined constant
    for i in range(len(texture_list)):
       texture_list[i] = pygame.transform.scale(texture_list[i], 
                                                SPRITE_CELL_DIMENSIONS)

    return texture_list